# bigt

[![Downloads](https://static.pepy.tech/personalized-badge/Bigt?period=total&left_color=grey&right_color=blue&left_text=Downloads)](https://pepy.tech/project/Bigt)
[![License](https://img.shields.io/static/v1?label=license&message=MIT&color=blueviolet)](https://en.wikipedia.org/wiki/MIT_License)
[![Doc](https://img.shields.io/static/v1?label=doc&message=io&color=blue)](https://vladku.gitlab.io/bigt/)

Bigt is a Python library for compare or merge nested unsorted dictionaries.

Examples:

```python
from bigt.dictext import DictExt

a = {"a": {"a": "str", "b": 123}}
b = {"a": {"a": "str", "b": 1234}}
result, not_present, changed = DictExt(b).issubset(a)
assert not result
assert changed == {"a": {"b": {"old": 1234, "new": 123} } }
assert not not_present
```

```python
>>> DictExt({"a": 3, "b": [{"b": 123}]}) + DictExt({"a": 1, "c": False, "b": [{"a": 12.64}]})
{'a': 1, 'b': [{'b': 123, 'a': 12.64}], 'c': False}
```

```python
>>> DictExt({"aa": 3, "a": [{"b": {"bv": 1, "bc": 2}, "a": 1}, [3,1,8,5]]}).sort()
{'a': [[1, 3, 5, 8], {'a': 1, 'b': {'bc': 2, 'bv': 1}}], 'aa': 3}
```

## Show difference visually

Install `rich` python library or use `pip install Bigt[rich]`

```python
from bigt.pprint import pprint

a = {"a":[{'z': {'a': '21', 'b': False, 'd': '20'}}, {'z': {'a': '1', 'b': False, 'd': '2'}}]}
b = {"a":[{'z': {'a': '1', 'b': False, 'd': {"a": '20'}, "y": {"a": 2}}}]}

pprint(b, a, weights={"a": {'z': {"a": 2}}})
```
![](./docs/source/_static/1.png)

```python
from bigt.pprint import pprint

a = {"a":[{'z': {'a': '21', 'b': False, 'd': '20'}}, {'z': {'a': '1', 'b': False, 'd': '2'}}]}
b = {"a":[{'z': {'a': '1', 'b': False, 'd': {"a": '20'}, "y": {"a": 2}}}]}

pprint(a, b, weights={"a": {'z': {"a": 2}}})
```
![](./docs/source/_static/2.png)

### Assert on difference found

```python
from bigt.pprint import assert_issubset

a = {"a":[{'z': {'a': '21', 'b': False, 'd': '20'}}, {'z': {'a': '1', 'b': False, 'd': '2'}}]}
b = {"a":[{'z': {'a': '1', 'b': False, 'd': {"a": '20'}, "y": {"a": 2}}}]}

assert_issubset(a, b, weights={"a": {'z': {"a": 2}}})
