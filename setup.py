import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name='Bigt',
    version='2022.1.4',
    author="Vladyslav Kurovskyi",
    description='Bigt is a Python library for compare or merge nested unsorted dictionaries.',
    long_description=long_description,
    long_description_content_type="text/markdown",
    py_modules=["bigt"],
    url="https://gitlab.com/vladku/bigt",
    packages=setuptools.find_packages(),
    extras_require={
        "rich": ["rich"]
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
        "Intended Audience :: Developers",
    ],
)
